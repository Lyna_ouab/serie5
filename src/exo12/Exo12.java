package exo12;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

public class Exo12 {

	public static void main(String[] args) {
		List<String> words = Arrays.asList("one","two","three","four",
							"five","six","seven","height","nine","ten",
							"eleven","twelve");
		System.out.println("The content of the list:");
		words.forEach((String word) -> System.out.println("• " + word));
		
		List<String> NewWords = new ArrayList<>(words);
		NewWords.removeIf((String word) -> word.length()%2 == 0);
		System.out.println("\nThe content of the list after removing:");
		NewWords.forEach((String word) -> System.out.println("◄ " + word));
		
		System.out.println("\nThe new list after toUpper:");
		Function<String, String> toUpper =
				s -> s.toUpperCase();
		for (String word : NewWords) {
			System.out.println("○ " + toUpper.apply(word));
		}
				
		Predicate<String> startsWithVowel =
				s -> s.charAt(0) == 'a' || s.charAt(0) == 'e'|| s.charAt(0) == 'y'||
				s.charAt(0) == 'u' || s.charAt(0) == 'i'|| s.charAt(0) == 'o';
		
		Function<String, String> vowelsToUpper =
				s -> startsWithVowel.test(s)? toUpper.apply(s):s;
				
		List<String> toUpperWords = new ArrayList<String>(words);
		System.out.println("\nThe list after vowelsToUpper:");
		for (String word : toUpperWords) {
			System.out.println("► " + vowelsToUpper.apply(word));
		}
		
		//Comparator:
		Comparator<String> compareLength = 
				Comparator.comparing(String ::length);
		System.out.println("\n Sorted list by length:");
		words.sort(compareLength);
		for (String word : words) {
			System.out.println("☼ " + word);			
		}
		
		Comparator<String> cmpLexic = 
				Comparator.comparing(String::toString);
		Comparator<String> cmpLengthThenLexic = 
				compareLength.thenComparing(cmpLexic);
		
		words.sort(cmpLengthThenLexic);
		System.out.println("\n Sorted list by length and the by age:");
		for (String word : words) {
			System.out.println("→ " + word);			
		}
	}

}
