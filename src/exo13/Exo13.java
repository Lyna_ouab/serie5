package exo13;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Exo13 {

	public static void main(String[] args) {
		Map<Integer, String> immutableMap = Map.ofEntries(
				Map.entry(3, "one"),
				Map.entry(4, "four"),
				Map.entry(5, "three"),
				Map.entry(6, "eleven")
				);

		Map<Integer, String> map = new HashMap<>(immutableMap);
		System.out.println("The immutableMap:");
		map.forEach((key,value) -> System.out.println("► key = " + key + ", value = " + value));
		map.replaceAll((key,value) -> value.toUpperCase());
		System.out.println("\nThe map after toUpperCase");
		map.forEach((key,value) -> System.out.println("◄ key = " + key + ", value = " + value));
	
		List<Integer> keys = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8);
		System.out.println("\nThe new map:");
		for (int key : keys) {
			map.putIfAbsent(key, "");
			System.out.println("→ key = " + key + ", value = " + map.get(key));
		}
		
		List<String> words = Arrays.asList("one","two","three","four",
				"five","six","seven","height","nine","ten",
				"eleven","twelve");
		Map <Integer, List<String>> mapList = new HashMap<>();
		for (String word : words) {
			int key = word.length();
			mapList.computeIfAbsent(key, k -> new ArrayList<String>())
				   .add(word);
		}
		
		System.out.println("\nThe map with the list of String:");
		mapList.forEach((key,value) -> System.out.println("◄ key = " + key + ", value = " + value));
		
		Map<Integer, String> mapMerge = new HashMap<>();
		for (String word : words) {
			int key = word.length();
			mapMerge.merge(key, word,
					(Value, newValue) -> Value + ", " + newValue);
		}
		System.out.println("\nThe map with String:");
		mapList.forEach((key,value) -> 
			System.out.println("► key = " + key + ", value = " + value));
	}

}
